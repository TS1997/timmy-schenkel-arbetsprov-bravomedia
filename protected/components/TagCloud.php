<?php

Yii::import('zii.widgets.CPortlet');

class TagCloud extends CPortlet
{
	public $title='Tags';
	public $maxTags=20;

	protected function renderContent()
	{
		$tags=Tag::model()->findTagWeights($this->maxTags);
		$i = 0;
		$len = count($tags);
		foreach($tags as $tag=>$weight)
		{
			if ($i != $len - 1) {
				$tag .= ",";
			}
			$link=CHtml::link(CHtml::encode($tag), array('post/index','tag'=>$tag));
			echo CHtml::tag('span', array(
				'class'=>'tag',
				//'style'=>"font-size:{$weight}pt",
			), $link)."\n";
			$i++;
		}
	}
}