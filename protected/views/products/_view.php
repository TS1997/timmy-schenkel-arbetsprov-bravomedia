<?php
/* @var $this ProductsController */
/* @var $data Products */
?>

<?php
	$pageSize = $widget->dataProvider->getPagination()->pageSize; 
?>
 
<?php if($index == 0) echo '<div class="row">'; ?>
 
<div class="view">
	<div class="image">
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/<?php echo CHtml::encode($data->image); ?>" alt="">
	</div>

	<p><?php echo CHtml::encode($data->name); ?></p>

	<a href="#">shop now</a>

</div>
 
<?php if($index != 0 && $index != $pageSize && ($index + 1) % 3 == 0)
    echo '<div class="clearfix"></div></div><div class="row">'; ?>
 
<?php if(($index + 1) == $pageSize ) echo '</div>'; ?>
