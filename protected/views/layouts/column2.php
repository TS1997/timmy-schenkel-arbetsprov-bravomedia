<?php $this->beginContent('/layouts/main'); ?>
	<div class="col-content">
		<?php echo $content; ?>
	</div><!-- End col-content -->
	<div class="sidebar">
		<?php if(!Yii::app()->user->isGuest) $this->widget('UserMenu'); ?>

		<?php $this->widget('TagCloud', array(
			'maxTags'=>Yii::app()->params['tagCloudCount'],
		)); ?>

		<?php $this->widget('RecentComments', array(
			'maxComments'=>Yii::app()->params['recentCommentCount'],
		)); ?>
	</div><!-- End sidebar -->
	<div class="clearfix"></div>
<?php $this->endContent(); ?>