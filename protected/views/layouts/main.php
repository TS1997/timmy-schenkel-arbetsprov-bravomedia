<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main2.css">
</head>
<body>
	<div class="page-row">
		<div class="header">
			<div class="logo">
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/bravomedia_black.png" alt="Bravomedia">
			</div><!-- End logo -->	

			<input type="checkbox" class="dropdown" id="dropdown">
			<label for="dropdown" class="dropdown-toggle"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/menu_icon.png"/></label>
			<div class="mainmenu">
				<?php
				$this->widget('zii.widgets.CMenu', array(
				   'activeCssClass'=>'active',
				   'activateParents'=>false,
				   'encodeLabel'=>false,
				   'htmlOptions'=>array('class'=>'menu'),
				   'items'=>array(
				   		array(
				   			'label'=>'Home',
				   			'url'=>array('post/index'),
				   			'itemOptions'=>array('class'=>'item'),
				   		),
				   		array(
				   			'label'=>'Products',
				   			'url'=>array('products/index'),
				   			'itemOptions'=>array('class'=>'item'),
				   		),	
				   	),
				));

				?>
			</div><!-- End main menu -->
		</div><!-- End header -->
	</div><!-- End page-row -->
	<hr>
	<div class="page-row expand">
		<div class="container">
			<?php echo $content; ?>
		</div><!-- End container -->
	</div><!-- End page-row -->
	<div class="page-row">
		<div class="footer">
			<div class="logo">
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/bravomedia_white.png" alt="Bravomedia">
			</div><!-- End logo -->
			<div class="copy">
				<p><span>Copyright &copy; <?php echo date('Y'); ?> by My Company. </span><span>All Rights Reserved. </span><span><?php echo Yii::powered(); ?></span></p>
			</div><!-- End copy -->
			<div class="clearfix"></div>
		</div><!-- End footer -->
	</div><!-- End page-row -->
</body>
</html>