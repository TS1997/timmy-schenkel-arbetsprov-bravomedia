<div class="form">
	<h1 class="commentTitle">Post a comment</h1>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'comment-form',
	'enableAjaxValidation'=>true,
)); ?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>
	
	<div class="row">
		<div class="threeCol">
			<?php echo $form->labelEx($model,'author'); ?>
			<?php echo $form->textField($model,'author',array('size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'author'); ?>
		</div>

		<div class="threeCol">
			<?php echo $form->labelEx($model,'email'); ?><br>
			<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>

		<div class="threeCol">
			<?php echo $form->labelEx($model,'url'); ?>
			<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'url'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="row">
		<div class="oneCol">
			<?php echo $form->labelEx($model,'content'); ?>
			<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($model,'content'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save'); ?>
	</div>
	<div class="clearfix"></div>

<?php $this->endWidget(); ?>

</div><!-- form -->