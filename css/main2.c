@font-face {
  font-family: "Lato";
  font-style: normal;
  font-weight: 400;
  src: local("Lato Regular"), local("Lato-Regular"), url(https://fonts.gstatic.com/s/lato/v11/1YwB1sO8YE1Lyjf12WNiUA.woff2) format("woff2"); }
body {
  margin: 0;
  padding: 0;
  font: normal 10pt Lato, Arial; }

.container {
  width: 90%;
  margin: 0px auto; }

.clearfix {
  display: none; }
  .clearfix:after {
    content: "";
    display: table;
    clear: both; }

hr {
  width: 100vw;
  margin: 0;
  margin-left: calc(-1 * ((100vw - 100%) / 2));
  border: none;
  height: 3px;
  color: #F7F7F7;
  background-color: #F7F7F7; }

.header {
  margin: 20px 0;
  font-weight: bold; }
  .header .logo {
    display: inline-block; }
  .header .mainmenu {
    float: right;
    display: inline-block; }
    .header .mainmenu ul {
      text-align: center;
      list-style-type: none;
      float: left;
      width: 100%;
      margin: 0; }
      .header .mainmenu ul li {
        float: left;
        width: auto;
        padding: 10px;
        text-transform: uppercase;
        font-size: 18px; }
        .header .mainmenu ul li a {
          text-decoration: none;
          color: #000; }
      .header .mainmenu ul li.active {
        border-bottom: 3px solid #E85D22; }

.products {
  width: 100%; }
  .products .row {
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin: 50px 0; }
    .products .row .view {
      float: left;
      text-align: center; }
      .products .row .view .image {
        background-color: #F7F7F7;
        width: 100%; }
        .products .row .view .image img {
          width: 350px;
          height: 350px; }
      .products .row .view p {
        text-transform: uppercase;
        font-weight: bold; }
      .products .row .view a {
        background-color: #E85D22;
        color: #fff;
        padding: 5px 10px;
        border: none;
        border-radius: 2px;
        text-decoration: none; }
  .products .products:before, .products .products:after {
    content: " ";
    display: table; }
  .products .products:after {
    clear: both; }

.footer {
  background-color: #000;
  padding: 5px 0; }
  .footer .container {
    margin: 10px auto;
    position: relative; }
    .footer .container .logo {
      float: left;
      position: absolute;
      top: 25%;
      left: 0; }
      .footer .container .logo img {
        display: inline-block;
        vertical-align: middle; }
    .footer .container .right {
      float: right;
      color: #fff; }
  .footer .clearfix {
    display: block; }

/*# sourceMappingURL=main2.c.map */
